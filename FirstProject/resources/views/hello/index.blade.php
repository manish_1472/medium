@extends('master')
@section('title','Hello Index')
@section('content')
<div class="container">
  <center>
  <div class="content">
    <p>CodeChef was created as a platform to help programmers make it big in the world of algorithms, computer programming and programming contests. At CodeChef we work hard to revive the geek in you by hosting a programming contest at the start of the month and another smaller programming challenge in the middle of the month. We also aim to have training sessions and discussions related to algorithms, binary search, technicalities like array size and the likes. Apart from providing a platform for programming competitions, CodeChef also has various algorithm tutorials and forum discussions to help those who are new to the world of computer programming.<br>Try your hand at one of our many practice problems and submit your solution in a language of your choice. Our programming contest judge accepts solutions in over 35+ programming languages. Preparing for coding contests were never this much fun! Receive points, and move up through the CodeChef ranks. Use our practice section to better prepare yourself for the multiple programming challenges that take place through­out the month on CodeChef. </p>
  </div>
  <ul style="text-align:inline;">
    @foreach($actions as $action)
      <a href="{!! action('HelloController@show',lcfirst($action->name)) !!}">{{$action->name}}</a>
    @endforeach
  </ul>
  <br>
  @if(count($errors)>0)
    <div>
      <ul>
        @foreach($errors->all() as $error)
          {{$error}}
        @endforeach
      </ul>
    </div>
  @endif
  <form role="form" action="/Laravel/FirstProject/public/hello/benice" method="post">
    <ul style="text-align:inline;">
    <div class="form-group col-md-4">
      <label for="action" class="col-md-6">Name Of Action</label>
      <input type="text" class="form-control col-md-6" name="name" id="action">
    </div>
    <div class="form-group col-md-4">
      <label for="nice" class="col-md-6">Niceness</label>
      <input type="text" class="form-control col-md-6" name="niceness" id="nice">
    </div>
    <div class="form-group col-md-4">
      <br>
      <button type="submit" class="btn btn-default">Do a nice action!!!!</button>
    </div>
    <input type="hidden" value="{{Session::token()}}" name="_token">
  </ul>
  </form>
  </center>
</div>
@endsection
