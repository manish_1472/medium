<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\NiceAction;
use App\NiceActionLog;
use App\Http\Requests;

class HelloCOntroller extends Controller
{
    public function index()
    {
      $actions = NiceAction::all();
      return view('hello.index',['actions'=>$actions]);
    }
    public function greed()
    {
      return view('hello.greed');
    }
    public function hug()
    {
      return view('hello.hug');
    }
    public function kiss()
    {
      return view('hello.kiss');
    }
    public function submit(Request $request)
    {
        $this->validate($request,[
          'name'=> 'required|alpha|unique:nice_actions',
          'niceness' => 'required|numeric',
        ]);
        $action = new NiceAction();
        $action->name =  $request['name'];
        $action->niceness =$request['niceness'];
        $action->save();
        return redirect(action('HelloController@index'));
    }
    private function transform($name)
    {
      $prefix='KING';
      return $prefix.' '.strtoupper($name);
    }
    public function show($name)
    {
      $action = "You";
      return view('hello.nice',['action'=>$name,'name'=>$action]);
    }
}
