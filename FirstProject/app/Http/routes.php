<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Route::get('/', function () {
    //return view('welcome');
//});
/*Route::group(['middleware' => ['web']], function () {
    Route::get('/','PagesController@home');
    Route::get('/contact','TicketController@create');
    Route::get('/about','PagesController@about');
    Route::post('/contact','TicketController@store');
    Route::get('/tickets','TicketController@index');
    Route::get('/ticket/{slug}', 'TicketController@show');
    Route::get('/ticket/{slug}/edit','TicketController@edit');
    Route::post('/ticket/{slug}/edit','TicketController@update');
    Route::post('/ticket/{slug}/delete','TicketController@destroy');
    Route::group(['prefic'=>'hello'],function(){
    Route::get('/hello','HelloController@index')->name('home');
    Route::get('/hello/{name}/benice','HelloController@show');
    Route::get('/hello/greed','HelloController@greed');
    Route::get('/hello/hug','HelloController@hug');
    Route::get('/hello/kiss','HelloController@kiss');
    Route::post('/hello/benice','HelloController@submit');
    Route::get('/sendemail', function () {

    $data = array(
        'name' => "Learning Laravel",
    );

    Mail::send('emails.welcome', $data, function ($message) {

        $message->from('from@example.com', 'Learning Laravel');

        $message->to('manishyuijaiswal@gmail.com')->subject('Learning Laravel test email');

    });

    return "Your email has been sent successfully";

  });
  });

});
*/
Route::get('/',function(){
  return view('medium.authentication');
});
Route::get('/callback',function(){
  return view('medium.callback');
});
Route::get('/create_post',function(){
  return view('medium.create_post');
});
